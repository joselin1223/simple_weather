<?php

namespace App\Http\Component;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

/**
 * WeatherApiEndPoint
 *
 * WeatherApiEndPoint component for the controller
 */
trait WeatherApiEndPoint
{
    /**
     * getWeatherForecasts
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function getWeatherForecasts(Request $request)
    {
        // Get Weather forecasts
        $response = Http::get(
            env('WEATHER_API_ENDPOINT') . '/onecall',
            [
                'appid' => env('WEATHER_API_KEY'),
                'lat' => $request->lat,
                'lon' => $request->lon,
                'exclude' => 'minutely,hourly',
                'units' => 'metric',
            ]
        );
        $arrayResponse = json_decode($response->getBody(), true);
        // Check api status code response
        if ($response->status() === 200) {
            $currentWeather = collect($arrayResponse['current']['weather'])->first();
            $currentDate = Carbon::createFromTimestamp($arrayResponse['current']['dt']);
            // Get daily weatherforecasts
            $daily = collect($arrayResponse['daily'])
                ->map(function ($entity) {
                    $dailyWeather = collect($entity['weather'])->first();
                    $dailyDate = Carbon::createFromTimestamp($entity['dt']);
                    // Return daily forecast
                    return [
                        'name' => $dailyWeather['main'],
                        'daily_name' => $dailyDate->format('l'),
                        'date' => $dailyDate->format('F d'),
                        'temperature' => $entity['temp']['day'] . " °C",
                        'wind' => $entity['wind_speed'] . " km/h",
                        'humidity' => $entity['humidity'] . " %",
                        'rain' => (isset($entity['rain']) ? $entity['rain'] : 0) . " %",
                        'image' => "http://openweathermap.org/img/wn/" . $dailyWeather['icon'] . ".png",
                    ];
                })
                ->toArray();
            // Remove current forecast
            unset($daily[0]);
            // Return success
            return [
                'status_code' => $response->status(),
                'data' => [
                    'current' => [
                        'name' => $currentWeather['main'],
                        'daily_name' => $currentDate->format('l'),
                        'date' => $currentDate->format('F d'),
                        'temperature' => $arrayResponse['current']['temp'] . " °C",
                        'wind' => $arrayResponse['current']['wind_speed'] . " km/h",
                        'humidity' => $arrayResponse['current']['humidity'] . " %",
                        'image' => "http://openweathermap.org/img/wn/" . $currentWeather['icon'] . ".png",
                    ],
                    'daily' => $daily,
                ],
            ];
        }
        // Return error
        return [
            'status_code' => $response->status(),
            'message' => $arrayResponse['message'],
        ];
    }
}
