<?php

namespace App\Http\Component;

/**
 * Response
 *
 * Response component for the controller
 */
trait Response
{
    /**
     * setResponseBody private Method
     * This creates a response blueprint
     *
     * @param array $body response body
     * @return object response
     */
    private function responseBody($body)
    {
        $response = response(json_encode($body, JSON_UNESCAPED_UNICODE), $this->code)
            ->header('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Success response
     *
     * @param array $data - data being returned
     * @return response
     */
    public function success($data = [])
    {
        $body = [
            'status_code' => 200,
            'message' => 'success',
        ];
        if ($data) {
            $body['data'] = $data;
        }
        $this->code = 200;

        return $this->responseBody($body);
    }

    /**
     * Custom Error response
     *
     * @param array $response
     * @return response
     */
    public function customResponse(array $response)
    {
        $this->code = $response['status_code'];

        return $this->responseBody($response);
    }
}
