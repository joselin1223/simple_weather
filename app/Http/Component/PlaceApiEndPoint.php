<?php

namespace App\Http\Component;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

/**
 * PlaceApiEndPoint
 *
 * PlaceApiEndPoint component for the controller
 */
trait PlaceApiEndPoint
{
    /**
     * getPlaces
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function getPlaces(Request $request)
    {
        $response = Http::withHeaders(['Authorization' => env('PLACE_API_KEY')])
            ->get(
                env('PLACE_API_ENDPOINT') . '/places/search',
                [
                    'limit' => 10,
                    'near' => $request->near,
                ]
            );
        $arrayResponse = json_decode($response->getBody(), true);
        // Check api status code response
        if ($response->status() === 200) {
            // Api return empty results
            if (empty($arrayResponse['results'])) {
                // Return error
                return [
                    'status_code' => 404,
                    'message' => 'Not Found',
                ];
            }
            $places = collect($arrayResponse['results'])
                ->map(function ($entity) {
                    // Get places
                    return [
                        'address' => $entity['location']['formatted_address'],
                        'lat' => $entity['geocodes']['main']['latitude'],
                        'lon' => $entity['geocodes']['main']['longitude'],
                    ];
                })
                ->reject(function ($value) {
                    // Remove empty address
                    return $value['address'] == '';
                })
                ->toArray();
            // Response success
            return [
                'status_code' => $response->status(),
                'data' => $places,
            ];
        }
        // Return error
        return [
            'status_code' => $response->status(),
            'message' => $arrayResponse['message'],
        ];
    }
}
