<?php

namespace App\Http\Middleware;

use App\Http\Component\Response;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * CheckAppToken
 *
 * Handle Application token
 */
class CheckAppToken
{
    use Response;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->hasHeader('API-KEY')) {
            $appToken = $request->header('API-KEY');
            if (Hash::check(env('API_TOKEN'), $appToken)) {
                // Continue
                return $next($request);
            }
            // Return error
            return $this->customResponse(['status_code' => 400, 'message' => 'Invalid Token']);
        } else {
            // Return error
            return $this->customResponse(['status_code' => 404, 'message' => 'Token not found']);
        }
        // Continue
        return $next($request);
    }
}
