<?php

namespace App\Http\Controllers\Api;

use App\Http\Component\Response;
use App\Http\Controllers\Controller;

/**
 * InvalidRequestController
 *
 * Handle invalid Request
 */
class InvalidRequestController extends Controller
{
    use Response;

    /**
     * return
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function return()
    {
        return $this->customResponse(
            [
                'status_code' => '404',
                'message' => 'Request not found',
            ]
        );
    }
}
