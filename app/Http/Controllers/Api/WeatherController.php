<?php

namespace App\Http\Controllers\Api;

use App\Http\Component\Response;
use App\Http\Component\WeatherApiEndPoint;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * WeatherController
 *
 * Handle all request about Weather
 */
class WeatherController extends Controller
{
    use Response;
    use WeatherApiEndPoint;

    /**
     * getForecasts
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getForecasts(Request $request)
    {
        $response = $this->getWeatherForecasts($request);
        if ($response['status_code'] === 200) {
            // Return success
            return $this->success($response);
        }
        // Return error
        return $this->customResponse($response);
    }
}
