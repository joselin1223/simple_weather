<?php

namespace App\Http\Controllers\Api;

use App\Http\Component\Response;
use App\Http\Component\PlaceApiEndPoint;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * PlaceController
 *
 * Handle all request about Place
 */
class PlaceController extends Controller
{
    use PlaceApiEndPoint;
    use Response;

    /**
     * getLocations
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLocations(Request $request)
    {
        $response = $this->getPlaces($request);
        if ($response['status_code'] === 200) {
            // Return success
            return $this->success($response);
        }
        // Return error
        return $this->customResponse($response);
    }
}
