<?php

use App\Http\Controllers\Api\InvalidRequestController;
use App\Http\Controllers\Api\PlaceController;
use App\Http\Controllers\Api\WeatherController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Locations
 */
Route::group(['prefix' => 'location'], function () {
    Route::get('/places', [PlaceController::class, 'getLocations']);
});

/**
 * Weather
 */
Route::group(['prefix' => 'weather'], function () {
    Route::get('/forecasts', [WeatherController::class, 'getForecasts']);
});

Route::fallback(
    function () {
        $invalid = new InvalidRequestController();
        // Return error
        return $invalid->return();
    }
);
