REQUIREMENTS = docker docker-compose vi
DOCKER_CONTAINER = weather

preflight:
	$(foreach REQUIREMENT, $(REQUIREMENTS), \
		$(if $(shell command -v $(REQUIREMENT) 2> /dev/null), \
			$(info Found `$(REQUIREMENT)`), \
			$(error Please install `$(REQUIREMENT)`) \
		) \
	)

# Build Project
setup: preflight
	docker-compose up -d --build
	cp .env.local .env
	docker-compose exec $(DOCKER_CONTAINER) composer install
	docker-compose exec $(DOCKER_CONTAINER) php artisan config:clear
	docker-compose exec $(DOCKER_CONTAINER) chmod -R 777 ./
	npm install && npm run dev