<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" type="text/css">
    <title>Weather</title>
    <script>
        const token = '$2y$10$qP24/60QjztG4faWWAS2su7HoSADNbkbpXPRYjrTGPwwkcv2hXi1y';
    </script>
</head>

<body>
    <main id="app">
        @yield('content')
    </main>

    <script src="{{ mix('js/app.js') }}"></script>
    @stack('custom-scripts')
</body>
<p class="text-center mt-3">© Joselin T. Macayanan</p>
</html>
