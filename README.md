# Simple Weather App

Bitbucket - https://bitbucket.org/joselin1223/simple_weather/src

---

## Setup
- Clone the repository `git clone https://joselin1223@bitbucket.org/joselin1223/simple_weather.git`
- Execute `make setup`

## Description
- PSR1, PSR4, PSR12 are included in the pre-commit, You will not be able to commit your updates if your code does not follow the coding standards
- Easy to setup just one command then you are good to go